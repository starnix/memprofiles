FROM oraclelinux:7
ENV DEBUGINFO_REPO https://oss.oracle.com/ol7/debuginfo/
RUN yum -y install oracle-epel-release-el7 &&\
    yum repolist &&\
    yum -y install git-core

VOLUME /mnt

ADD ./oracle7.sh /oracle7.sh
ENTRYPOINT ["/oracle7.sh"]
