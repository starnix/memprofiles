FROM centos:centos7
ENV VOL3_PKG_URL http://debuginfo.centos.org/7/x86_64/
ENV VOL2_PKG_URL https://linuxsoft.cern.ch/cern/centos/7/updates/x86_64/Packages/
RUN yum -y install epel-release &&\
    yum repolist &&\
    yum -y install git-core

VOLUME /mnt

ADD ./centos7.sh /centos7.sh
ENTRYPOINT ["/centos7.sh"]
