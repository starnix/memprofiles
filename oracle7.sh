#!/usr/bin/env bash

if [ "${#}" -ge 1 ]; then
    exec "${@}"
fi

if [ -z $KERNEL_VERSION ]; then
    echo -e "KERNEL_VERSION emtpy.  Please pass the version as the env variable KERNEL_VERSION"
    exit 1
fi

function vol3_install_kernel {
    echo -e "Installing needed Kernel Packages...\n\n" && \
    yum -y install ${DEBUGINFO_REPO}/kernel-debuginfo-common-x86_64-${KERNEL_VERSION}.rpm &&\
	yum -y install ${DEBUGINFO_REPO}/kernel-debuginfo-${KERNEL_VERSION}.rpm &&\
	yum -y install kernel-debug-${KERNEL_VERSION} && return 0 || return 1
}

function vol2_install_kernel {
    echo -e "Installing Kernel....\n\n" && \
    yum -y install kernel-debug-${KERNEL_VERSION} kernel-debug-devel-${KERNEL_VERSION} && return 0 || return 1
}


function vol3_deps {
    echo -e "Installing Vol3 Memprofile deps...\n\n" && \
    yum -y install golang golang-bin golang-x-sys-devel  &&\
	git clone https://github.com/volatilityfoundation/dwarf2json.git /opt/dwarf2json  &&\
	(cd /opt/dwarf2json; go build) && return 0  || return 1
}

function vol2_deps {
    echo -e "Installing Vol2 Memprofile deps...\n\n" && \
    yum -y install libdwarf libdwarf-tools gcc gcc-c++ make zip &&\
	git clone https://github.com/volatilityfoundation/volatility.git /opt/volatility && return 0 || return 1
}

case $VOLVER in
    vol2)
	if vol2_install_kernel; then
	    if vol2_deps; then
		(cd /opt/volatility/tools/linux; KVER=${KERNEL_VERSION}.debug make)
		(cd /opt/volatility/tools/linux;zip /mnt/${KERNEL_VERSION}.zip module.dwarf /boot/System.map-${KERNEL_VERSION}.debug )
	    else
		echo "Failed to install Volatility 2 profile deps"
		exit 1
	    fi
	else
	    echo "Failed to install needed Kernel packages."
	    exit 1
	fi
	;;
    vol3)
	if vol3_install_kernel; then
	    if vol3_deps; then
		/opt/dwarf2json/dwarf2json linux --elf /usr/lib/debug/usr/lib/modules/${KERNEL_VERSION}/vmlinux --system-map /boot/System.map-${KERNEL_VERSION}.debug | xz > /mnt/${KERNEL_VERSION}.json.xz
	    else
		echo "Failed to install volatility3 sysmbols deps"
		exit 1
	    fi
	else
	    echo "Failed to install Kernel"
	    exit 1
	fi
	;;
    *)
	echo "Choose either vol2 or vol3 profiles"
	exit 1
esac

exit 0
